package com.ydq.entity;

/**
 * 电影信息表 实体类
 *
 * @author ydq
 * @since 2022-8-22 16:33:49
 */
public class Movie {

    /**
     * 序号
     */
    private Long id;

    /**
     * 影视名称
     */
    private String movieName;

    /**
     * 链接地址
     */
    private String url;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 影视类型
     */
    private String movieType;

    /**
     * 更新时间
     */
    private Integer updateTime;

    /**
     * 创建时间
     */
    private Integer clientId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getMovieType() {
        return movieType;
    }

    public void setMovieType(String movieType) {
        this.movieType = movieType;
    }

    public Integer getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }
}
