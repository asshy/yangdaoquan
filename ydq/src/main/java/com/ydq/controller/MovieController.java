package com.ydq.controller;

import com.ydq.common.QueryPage;
import com.ydq.dao.MovieDao;
import com.ydq.entity.Movie;
import com.ydq.service.MovieService.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 电影区 Controller
 *
 * @author ydq
 * @since 2022-8-22 16:33:49
 */
@RestController
@RequestMapping(value = "/ydq/movie")
public class MovieController {

    @Autowired
    private MovieDao movieDao;

    @PostMapping(value = "/getMovieByPage")
    public GetMovieByPageOutput getMovieByPage(@RequestBody GetMovieByPageInput input) {
        GetMovieByPageOutput output = new GetMovieByPageOutput();
        QueryPage<Movie> page = movieDao.getMovieInfoByPage(input.getMovieName(), input.getMovieType(),
                input.getPageNo(), input.getPageSize());
        List<MovieDTO> movieDTOList = page.getList().stream().map(movie -> {
            MovieDTO movieDTO = new MovieDTO();
            BeanUtils.copyProperties(movie, movieDTO);
            return movieDTO;
        }).collect(Collectors.toList());
        output.setCurrentPage(page.getCurrentPage());
        output.setTotal(page.getTotal());
        output.setRows(movieDTOList);
        return output;
    }

}
