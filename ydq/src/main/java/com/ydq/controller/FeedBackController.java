package com.ydq.controller;

import com.ydq.dao.FeedbackDao;
import com.ydq.operator.maintainance.center.demo.service.JWTService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;

/**
 * 反馈 Controller
 *
 * @author ydq
 * @since 2022-8-22 16:33:49
 */
@RestController
@RequestMapping(value = "/ydq")
public class FeedBackController {

    @Autowired
    private FeedbackDao feedbackDao;

    @DubboReference(url = "dubbo://localhost:20881", version = "1.0.0", check = true)
    private JWTService jwtService;

    @PostMapping(value = "/feedback")
    public boolean feedback(@RequestBody FeedBackInput input) {
        // 解析userName
        String userName = jwtService.getUserName(input.getToken());

        // 获取时间
        Date time = Calendar.getInstance().getTime();

        // 插入数据库
        boolean result = feedbackDao.insertFeedback(input.getSuggestion(), time, userName);
        return result;
    }

}



class FeedBackInput {
    /**
     * 反馈意见
     */
    private String suggestion;
    /**
     * token
     */
    private String token;

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
