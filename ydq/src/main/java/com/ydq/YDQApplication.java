package com.ydq;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableDubbo(scanBasePackages = "com.ydq") // dubbo自动注册
@PropertySource("classpath:/application.properties") // 扫描配置文件地址
public class YDQApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(YDQApplication.class, args);
    }

}
