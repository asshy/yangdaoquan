package com.ydq.dao;

import java.util.Date;

/**
 * 反馈意见 Dao
 *
 * @author ydq
 * @since 2022年9月13日15:31:19
 */

public interface FeedbackDao {

    boolean insertFeedback(String suggestion, Date time, String userName);
}
