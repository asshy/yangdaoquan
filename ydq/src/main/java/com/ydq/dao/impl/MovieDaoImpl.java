package com.ydq.dao.impl;

import com.ydq.common.QueryPage;
import com.ydq.dao.MovieDao;
import com.ydq.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 电影区 Dao 实现类
 *
 * @author ydq
 * @since 2022-8-22 16:33:49
 */
@Component
public class MovieDaoImpl implements MovieDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public QueryPage<Movie> getMovieInfoByPage(String movieName, String movieType, Integer pageNo, Integer pageSize) {
        QueryPage<Movie> queryPage = new QueryPage<>();
        List<Object> params = new ArrayList<>();

        StringBuffer sql = new StringBuffer();
        sql.append("select * from movie where 1=1 ");
        if(!StringUtils.isEmpty(movieName)){
            sql.append("and movie_name like concat('%',?,'%') ");
            params.add(movieName);
        }
        if(!StringUtils.isEmpty(movieType)){
            sql.append("and movie_type=? ");
            params.add(movieType);
        }

        String countSql = "select count(0) from ( " + sql.toString() + ") a";
        Integer total = this.jdbcTemplate.queryForObject(countSql, Integer.class,
                params.toArray(new Object[params.size()]));
        queryPage.setTotal(total);

        if(pageNo != null && pageSize != null) {
            sql.append("limit ?, ?");
            params.add((pageNo-1)*pageSize);
            params.add(pageSize);
        }

        Object[] para = params.toArray(new Object[params.size()]);
        BeanPropertyRowMapper rowMapper = new BeanPropertyRowMapper(Movie.class);
        List<Movie> movies = this.jdbcTemplate.query(sql.toString(), rowMapper, para);
        queryPage.setList(movies);
        queryPage.setCurrentPage(pageNo);
        return queryPage;
    }
}
