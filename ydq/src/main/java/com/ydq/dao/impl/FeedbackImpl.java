package com.ydq.dao.impl;

import com.ydq.dao.FeedbackDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 反馈建议 Dao 实现类
 *
 * @author ydq
 * @since 2022年9月13日15:32:10
 */
@Component
public class FeedbackImpl implements FeedbackDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean insertFeedback(String suggestion, Date time, String userName) {
        return jdbcTemplate.update("insert into feedback values (?,?,?)", suggestion, time, userName) > 0;
    }
}
