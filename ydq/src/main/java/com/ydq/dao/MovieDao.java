package com.ydq.dao;

import com.ydq.common.QueryPage;
import com.ydq.entity.Movie;

/**
 * 电影区 Dao
 *
 * @author ydq
 * @since 2022-8-22 16:33:49
 */

public interface MovieDao {

    QueryPage<Movie> getMovieInfoByPage(String movieName, String movieType, Integer pageNo, Integer pageSize);

}
