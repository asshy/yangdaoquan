package com.ydq.service.impl;

import com.ydq.controller.MovieController;
import com.ydq.service.MovieService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 电影区服务类
 *
 * @author ydq
 * @since 2022-8-22 16:33:49
 */
@Component
@DubboService
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieController movieController;


    @Override
    public GetMovieByPageOutput getMovieByPage(GetMovieByPageInput input) {
        GetMovieByPageOutput output = movieController.getMovieByPage(input);
        return output;
    }
}
