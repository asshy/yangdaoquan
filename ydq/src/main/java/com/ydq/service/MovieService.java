package com.ydq.service;

import java.io.Serializable;
import java.util.List;

/**
 * 电影区服务类接口
 *
 * @author ydq
 * @since 2022-8-22 16:33:57
 */
public interface MovieService {

    GetMovieByPageOutput getMovieByPage(GetMovieByPageInput input);

    class MovieDTO {

        /**
         * 序号
         */
        private Long id;

        /**
         * 影视名称
         */
        private String movieName;

        /**
         * 链接地址
         */
        private String url;

        /**
         * 简介
         */
        private String introduction;

        /**
         * 简介
         */
        private String movieType;

        /**
         * 更新时间
         */
        private Integer updateTime;

        /**
         * 创建时间
         */
        private Integer createTime;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getMovieName() {
            return movieName;
        }

        public void setMovieName(String movieName) {
            this.movieName = movieName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getMovieType() {
            return movieType;
        }

        public void setMovieType(String movieType) {
            this.movieType = movieType;
        }

        public Integer getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Integer updateTime) {
            this.updateTime = updateTime;
        }

        public Integer getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Integer createTime) {
            this.createTime = createTime;
        }
    }

    class GetMovieByPageInput {
        /**
         * 电影名称
         */
        private String movieName;
        /**
         * 电影类型
         */
        private String movieType;

        /**
         * 页码
         */
        private Integer pageNo;

        /**
         * 每页数量
         */
        private Integer pageSize;

        public String getMovieName() {
            return movieName;
        }

        public void setMovieName(String movieName) {
            this.movieName = movieName;
        }

        public String getMovieType() {
            return movieType;
        }

        public void setMovieType(String movieType) {
            this.movieType = movieType;
        }

        public Integer getPageNo() {
            return pageNo;
        }

        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }
    }

    class GetMovieByPageOutput implements Serializable {
        private static final long serialVersionUID = 1L;

        /**
         * 错误代码
         */
        private Integer errorNo;

        /**
         * 错误提示
         */
        private String errorInfo;

        /**
         * 总条数
         */
        private Integer total;

        /**
         * 当前页码
         */
        private Integer currentPage;

        /**
         * 电影信息表DTO
         */
        private List<MovieDTO> rows;

        public Integer getErrorNo() {
            return errorNo;
        }

        public void setErrorNo(Integer errorNo) {
            this.errorNo = errorNo;
        }

        public String getErrorInfo() {
            return errorInfo;
        }

        public void setErrorInfo(String errorInfo) {
            this.errorInfo = errorInfo;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public List<MovieDTO> getRows() {
            return rows;
        }

        public void setRows(List<MovieDTO> rows) {
            this.rows = rows;
        }
    }
}
