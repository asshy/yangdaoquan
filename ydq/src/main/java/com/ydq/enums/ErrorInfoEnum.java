package com.ydq.enums;

public enum ErrorInfoEnum {

    /**
     * 用户名重复
     */
    REPEAT_USER(10001, "用户名已存在"),


    /**
     * 用户名或密码错误
     */
    WRONG_PASSWORD(10002, "用户名或密码错误"),
    ;

    private final Integer errorNo;
    private final String errorInfo;

    ErrorInfoEnum(Integer errorNo, String errorInfo){
        this.errorNo = errorNo;
        this.errorInfo = errorInfo;
    }

    public Integer getErrorNo() {
        return errorNo;
    }

    public String getErrorInfo() {
        return errorInfo;
    }
}
