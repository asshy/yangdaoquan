import request from './api'

// 登录
export function login (data) {
  const url = '/omc/user/login'
  return request.post(url, data)
}
// 登录
export function register (data) {
  const url = '/omc/user/register'
  return request.post(url, data)
}
// 查询海外剧信息
export function getMovieByPage (data) {
  const url = '/ydq/movie/getMovieByPage'
  return request.post(url, data)
}
// 校验token
export function verifyToken (data) {
  const url = '/omc/user/verify'
  return request.post(url, data)
}
// 反馈
export function feedback (data) {
  const url = '/ydq/feedback'
  return request.post(url, data)
}
