import RequestFactory from '../request/RequestFactory.js'
import { onRequest, onRequestError, onResponse, onResponseError } from '../request/interceptor.js'

// json请求
const request = RequestFactory.createJsonRequest()
// 请求拦截器
request.interceptors.request.use(onRequest, onRequestError)
request.interceptors.response.use(onResponse, onResponseError)

export default request
