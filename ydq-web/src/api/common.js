/**
 * 睡眠多少毫秒
 * @param {ms} d
 */
export function sleep (d) {
  var t = Date.now()
  while (Date.now - t <= d);
}
