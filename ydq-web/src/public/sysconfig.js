'use strict'

/**
 * 应用在运行状态下的全局配置；
 * 框架的全局配置应该挂载在唯一的命名空间下；
 */
(function (global) {
  global.FRAME_CONFIG = {
    CAS_LOGIN_URL: 'http://localhost:9000/'
  }
})(window)
