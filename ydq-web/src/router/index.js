import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '../view/HelloWorld.vue'
import frame from '../view/frame.vue'
import login from '../view/login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/frame',
      name: 'frame',
      component: frame
    },
    {
      path: '/',
      name: 'login',
      component: login
    }
  ]
})
