import axios from 'axios'
import { configJson } from './config'
import { MessageBox } from 'element-ui'

// 通用请求拦截器
export const onRequest = (config = {}) => {
  axios.post(configJson.baseURL + '/omc/user/verify', {
    'token': window.sessionStorage.getItem('token')
  },
  {
    headers: {
      // 切换content-type, transformRequest里面的方法也要相应的注释掉或不注释掉
      'Content-Type': 'application/json'
    }
  }).then((res) => {
    if (res) {
      if (res.data === false) {
        // token校验 失败或超时
        MessageBox.confirm('登录已失效，请重新登录', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          window.open('http://localhost:8080/#/', '_self')
        }).catch(() => {
        })
      }
    }
  }).catch(function (error) {
    console.log(error)
  })

  return config
}

// 通用请求错误时候
export const onRequestError = error => {
  Promise.reject(error)
}

// 通用响应
export const onResponse = response => {
  if (
    (response.data &&
      (response.data.errorNo === 9008 ||
        response.data.errorNo === 9007 ||
        response.data.errorNo === 9006 ||
        response.data.errorNo === 9005 ||
        response.data.errorNo === 9026 ||
        response.data.errorNo === 9004)) ||
    !response
  ) {
    let text = ''
    let flag = true
    let errorNo = response.data.errorNo
    if (errorNo === 9008 || errorNo === 9026) {
      text = '对不起,您缺少访问权限'
      this.$message.error(text)
    } else if (errorNo === 9006) {
      text = '页面已经失效,请先登录'
      flag = false
    } else if (errorNo === 9007) {
      text = '会话已失效,请重新登录'
      flag = false
    } else if (errorNo === 9004) {
      text = '您已被踢出,请先登录'
      flag = false
    } else if (errorNo === 9005) {
      text = '您已在别地方登录,请先登录'
      flag = false
    } else if (errorNo === 9021) {
      text = '锁屏状态不能操作，请先解锁'
      flag = false
    } else {
      flag = false
      if (response.data.errorInfo) {
        text = '异常错误：' + response.data.errorInfo
      } else {
        text = '系统出现异常'
      }
    }
    if (!flag && window.LOCAL_CONFIG.isToken) {
      this.$alert(text, '提示', {
        confirmButtonText: '确定',
        callback: () => {
          this.$router.push('/')
        }
      })
    }
  }
  return response
}

// 通用响应错误
export const onResponseError = error => {
  console.log('onResponseError')
  const response = error.response
  if (error && response && response.data && response.data.errorNo) {
    let flag = true
    let text = ''
    let tmpData = response.data.data ? response.data.data : response.data
    let data = tmpData instanceof Array ? tmpData[0] : tmpData
    let errorNo = data.errorNo ? data.errorNo : 9023
    if (errorNo === 9008 || errorNo === 9026) {
      text = '对不起,您缺少访问权限'
    } else if (errorNo === 9006) {
      text = '页面已经失效,请先登录'
      flag = false
    } else if (errorNo === 9007) {
      text = '会话已失效,请重新登录'
      flag = false
    } else if (errorNo === 9004) {
      text = '您已被踢出,请先登录'
      flag = false
    } else if (errorNo === 9005) {
      text = '您已在别地方登录,请先登录'
      flag = false
    } else if (errorNo === 9011) {
      text = '验证码出错'
    } else if (errorNo === 9021) {
      text = '锁屏状态不能操作，请先解锁'
    } else {
      if (data.errorInfo) {
        text = '系统异常：' + data.errorInfo
      } else if (data.errorNo) {
        text =
          '系统异常:' + (data.errorNo)
      } else {
        text = '系统异常:未知'
      }
    }
    if (!flag) {
      this.$alert(text, '提示', {
        confirmButtonText: '确定',
        callback: () => {
          this.$router.push('/')
        }
      })
    }
  }
  return Promise.reject(error)
}
