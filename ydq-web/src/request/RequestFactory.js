import axios from 'axios'
import { configJson } from './config'

class RequestFactory {
  // 常用json请求
  static createJsonRequest (options = {}) {
    if (typeof options !== 'object') {
      throw new Error('config option must be a object')
    }

    // 创建实例
    const instance = axios.create({
      ...configJson,
      ...options,
      headers: {
        // 防止后端打印警告： Content type 'application/x-www-form-urlencoded;charset=UTF-8' not supported
        'Content-Type': 'application/json'
      }
    })
    return instance
  }
}

export default RequestFactory
